import matplotlib.pyplot as plt
import os, random
import numpy as np


from util import Util

class Graph:

    def __init__(self, graph_path=None, default_directory=None, save_override=True, title=None):
        self.graph_path = graph_path
        self.default_directory = default_directory
        self.points = []
        self.circles = []
        self.save_override = save_override
        self._axes = { "x":{"min":None,"max":None}, "y":{"min":None,"max":None} }
        self.title = title
        self.override_axes = False

    def initialize(self):
        fig, self._ax = plt.subplots()

        # Default Directory
        if self.default_directory is None:
            self.default_directory = "pictures"
        
        Util.createFolder(self.default_directory)

        return self

    def add_legend(self):
        # if self.cluster_n < 10:
        if True:
            plt.legend(loc = 'lower right', fontsize=12)
        else:
            plt.legend(
                bbox_to_anchor=(0, -0.2),
                loc=3,
                ncol= 5,
                borderaxespad=0.)
    
    def add_point(self, point, name, lvl):
        self.points.append({
            "point": point,
            "name" : name,
            "lvl" : lvl
        })

    def remove_merged_cluster(self, cluster):
        points = [point["point"] for point in self.points]
        points = np.array(points)

        indexes_array = []

        for c in cluster["clusters"]:
            point_x = c["point"][0]
            point_y = c["point"][1]
            
            # gestion des X
            idxX = np.where( points[:,0] == point_x)[0]
            
            if idxX.shape[0] == 1:
                indexes = idxX
            else:
                # gestion des Y
                idxY = np.where(points[:,1] == point_y)[0]
                
                idx = np.append(idxX, idxY)

                idx_unique, counts = np.unique(idx, return_counts=True)

                indexes = idx_unique[ np.where(counts > 1)[0] ]

            for index in indexes:
                indexes_array.append(index)

        # Suppression des doublons
        indexes_array = list(set(indexes_array))
        # Trie par ordre croissant
        indexes_array.sort()
        # Invese le trie et donc trie par Ordre décroissant
        indexes_array = indexes_array[::-1]

        # Clear la self.points
        for index in indexes_array:
            del self.points[index]

        # Circles
        indexes = []
        for i, circle in enumerate(self.circles):
            for c in cluster["clusters"]:
                if circle["cluster.id"] == c["id"]:
                    indexes.append(i)
                    
                    break;
            

        # suppression de la list
        indexes = indexes[::-1]
        
        for index in indexes:
            del self.circles[index]


    def add_circle_from_cluster(self, cluster, lvl):
        min_x = None
        min_y = None

        max_x = None
        max_y = None
        
        points = Util.get_all_points( cluster["clusters"] )
        points = np.array( points )
            
        max_x = points[np.argmax(points[:,0])][0]
        max_y = points[np.argmax(points[:,0])][1]

        radius_x = max_x - cluster["point"][0]
        radius_y = max_y - cluster["point"][1]

        radius = (radius_x if radius_x > radius_y else radius_y)

        self.circles.append({
            "cluster.id" : cluster["id"],
            "x" : cluster["point"][0],
            "y" : cluster["point"][1],
            "raidus" : radius,
            "lvl" : lvl 
        })

    def save_graph(self):
        self._make_graph()

        plt.savefig( self._graph_path() )

        plt.close('all')

    def show_graph(self):
        self._make_graph()
        
        plt.show()
        plt.close('all')

    def _add_point(self, point, name, color):
        plt.scatter(point[0], point[1], color=color, label=name )

    def _add_circle(self, x, y, radius, color):
        # padding : affiche mieux les cercles
        radius += self._calc_padding()

        circle = plt.Circle((x, y), radius, color=color, fill=False)
        self._ax.add_artist(circle)

    def _make_graph(self):
        fig, self._ax = plt.subplots()

        max_lvl = None
        for element in self.points:
            lvl = element["lvl"]

            if max_lvl is None or lvl > max_lvl:
                max_lvl = lvl
            
        number = (max_lvl + 1) * 2
        colors = np.arange( number )
        norm = plt.Normalize(1, number)
        cmap = plt.cm.RdYlGn

        for element in self.points:
            color = cmap( norm( colors[ element["lvl"] * 2 ] ) )
            
            self._add_point( element["point"], element["name"], color )
        
        for circle in self.circles:
            color = cmap( norm( colors[ circle["lvl"] * 2 ] ) )

            self._add_circle( circle["x"], circle["y"], circle["raidus"], color )

        if self.override_axes:
            self._define_axes()

        if self.title is not None:
            self._set_title()

    def _define_axes(self):
        self._define_axes_values()
        
        plt.xlim(self._axes["x"]["min"]-1, self._axes["x"]["max"])
        plt.ylim(self._axes["y"]["min"]-1, self._axes["y"]["max"])
    #plt.xticks([]), plt.yticks([])
#plt.axis()
# if title is not None:
#     plt.title(title, fontsize=18)

    def _set_title(self):
        plt.title(self.title, fontsize=18)

    def _define_axes_values(self):
        for element in self.points:
            # X
            x = element["point"][0]

            # min
            if  self._axes["x"]["min"] is None or self._axes["x"]["min"] > x:
                self._axes["x"]["min"] = x

            # max
            if self._axes["x"]["max"] is None or x > self._axes["x"]["max"]:
                self._axes["x"]["max"] = x
            
            # Y
            y = element["point"][1]

            # min
            if self._axes["y"]["min"] is None or self._axes["y"]["min"] > y:
                self._axes["y"]["min"] = y

            # max
            if self._axes["y"]["max"] is None or y > self._axes["y"]["max"]:
                self._axes["y"]["max"] = y

    def _graph_path(self):
        if self.graph_path is not None:
            return self.graph_path

        elements = os.listdir(self.default_directory)

        name = "graph_" + str(len(elements)) + ".png"

        path = os.path.join( self.default_directory, name )

        if self.save_override:
            self.graph_path = path

        return path

    def _calc_padding(self):
        maxX = None
        for p in self.points:
            x = p["point"][0]

            if maxX is None or maxX < x:
                maxX = x

        return round( 0.3 * maxX / 10, 2 )