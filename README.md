# Processus
1. Récuperation du(des) vidéo(s)  
2. Extraction d'image  
3. Fitrer les images.
    * Garder uniquement les visages
    * Croper les images
    * Redimensionner les images (test effectué en 300x300)
    * Mettre la dimension ".png" (autre dimension non testé)
4. Passer un scharr algorithme sur les images
5. Recuperer pixel par pixel en effectuant un treshold sur le scharr algorithme
6. Limiter le nombre d'élément dans la RAM (maxi tester 50 * 25 * 300 * 300 * 3)  
    | Nombre | Description|
    | :-----:|:-----------|
    | 50     | Un nombre magique                  |
    | 25     | Nombre de personnes lors du test   |
    | 300    | Hauteur de chaque image            |
    | 300    | Largeur de chaque image            |
    | 3      | Nombre de channels de chaque image |
7. Application du TSNE algorithm
8. Application du AHC algorithm. *Cette algorithme a été écrit en interne*
9. Répartition des images selon les clusters crée par l'agorithm <small>(*précédant --> AHC*)</small>
10. Si des images dans les clusters sont connu alors **Labelisation** des clusters
11. Si une nouvelle vidéo est reçu procédé aux étapes [1, 2, 3, 4, 5] et si les images sont trouvé dans un cluster alors on connait la personne a l'intérrieur

# Requirement
* tester avec python 3.4.2
