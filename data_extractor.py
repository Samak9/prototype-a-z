import os, sys, re, json, csv
import numpy as np
from datetime import datetime

from array import *
from random import shuffle
from scipy import ndimage

from image_util import ImageUtil
from config import Config

class DataExtractor:
    # Todo supprimer les fonctions non utilisé


    # todo déplace cette fonction dans un fichier approprié
    # Extrait le nom des personnes depuis le chemin relatif ou absolue de l'image.
    # Le nom doit etre le dernier dossier contenant l'image
    @staticmethod
    def getPersonneName(filename):
        separator = "\\" if sys.platform == "win32" else "/"
        name = filename.replace("_", "").split(separator)[-2]

        # todo train, test et etc... doivent venir d'une conf
        if name == "test" or name == "train" or name=="croped" or name.startswith("images"):
            arr = filename.split(separator)[-1].split("_")
            name = arr[0] + "_" + arr[1] # todo appronfondir ce code. ne devrait pas marcher avec les gens qui on des noms composer ou s'ils ont une particule ("de")

            regex = r"[0-9]"
            matches = re.findall(regex, name)

            if len(matches) > 0:
                return None
        return name

    # todo déplace cette fonction dans un fichier approprié
    # Enregistrer les noms dans un fichier
    @staticmethod
    def saveNames(names):
        # todo utiliser config
        ImageUtil.createFolder("files") # todo utiliser config

        filename = os.path.join("files", "labels.txt") # todo utiliser config # todo mettre la bonne extention de fichier
        # todo utilisé une fonction global d'ecriture de json
        with open(filename, 'w') as file:
            file.write(json.dumps(names))  # use `json.loads` to do the reverse

        return names

    # todo déplace cette fonction dans un fichier approprié
    # Charge les noms issus du fichier
    @staticmethod
    def loadNames():
        # todo names utiliser config
        filename = os.path.join("files", "labels.txt")

        if not os.path.isfile(filename):
            return None
        else:
            json_data = open(filename).read()
            names = json.loads(json_data)

            return names

    # todo déplace cette fonction dans un fichier approprié
    # Recupere tous les noms des personnes
    @staticmethod
    def getNames(files):
        names = DataExtractor.loadNames()

        if names is not None:
            return names
        else:
            names = []

            for filePath in files:
                name = DataExtractor.getPersonneName(filePath)

                if name not in names:
                    names.append(name)
            DataExtractor.saveNames(names)

            return names


    @staticmethod
    def getData(filename_memmap, folder, dtype, doNormalize=False, doThreshold=False):
        print("GetData data_extractor") # folder : ", folder)
        separator = "\\" if sys.platform == "win32" else "/"
       
        # recuepre les confs
        config = Config()
        # imageConfig = Config.getImageConfig(["channels", "colored"])
        # channels = imageConfig[0]
        channels = config.image.channels
        # colored = imageConfig[1]
        colored = config.image.colored
        

        isRGB = channels == 3 and colored == "True"
        isHexa = channels == 1 and colored == "True"
        isCanny = colored == "Canny"
        isScharr = colored == "Scharr"
        isGrey = channels == 1 and colored == "Grey"

        # TEST
        # print("isRGB : ", isRGB)
        # print("isHexa : ", isHexa)
        # print("isCanny : ", isCanny)
        # print("isScharr : ", isScharr)
        # print("isGrey : ", isGrey)
        # exit()
        
        files = ImageUtil.getAllFiles(folder)

        if isRGB:
            data = DataExtractor.getDataRGB(filename_memmap, files)
            labels, data_image_path = DataExtractor.getLabels(files)
        elif isHexa:
            data = DataExtractor.getDataHexa(filename_memmap, files)
            labels, data_image_path = DataExtractor.getLabels(files)
        elif isCanny:
            data, labels, data_image_path = DataExtractor.getDataCannyAndLabels(filename_memmap, files)
        elif isScharr:
            data, labels, data_image_path = DataExtractor.getDataScharrAndLabels(filename_memmap, files, dtype, doNormalize=doNormalize, doThreshold=doThreshold)
        elif isGrey:
            data = DataExtractor.getDataGrey(filename_memmap, files, dtype, doNorme=doNormalize)
            labels, data_image_path = DataExtractor.getLabels(files)
        else:
            raise ValueError("DataExtractor pas dev")

        #return DataExtractor.DataImageFormat(data_image), DataExtractor.DataLabelFormat(data_label)
        return data, labels, data_image_path


    # todo make comment
    @staticmethod
    def getLabels(files):
        # generate names
        names = DataExtractor.getNames(files)

        labels = []
        data_image_path = []

        for filename in files:
            data_image_path.append(filename)

            personne_name = DataExtractor.getPersonneName(filename)
            label = names.index(personne_name)
            labels.append(label) # labels start (one unsigned byte each)

        return labels, data_image_path

    #todo make comment
    @staticmethod
    def getDataCannyAndLabels(filename_memmap_base, files):
        # Files
        filename_memmap_data = filename_memmap_base + "_data.dat"
        filename_memmap_labels = filename_memmap_base + "_labels.dat"
        filename_csv_impath = filename_memmap_base + "_impath.csv"

        fileExist = os.path.isfile(filename_memmap_data) and os.path.isfile(filename_memmap_labels) and os.path.isfile(filename_csv_impath)

        if fileExist:
            # Data
            data = DataExtractor.readMemmap(filename_memmap_data)
            # Label
            labels = DataExtractor.readMemmap(filename_memmap_labels)
            # ImPath
            impath_array = DataExtractor.readCSVFile(filename_csv_impath)
        else:
            # generate names
            names = DataExtractor.getNames(files)

            separator = "\\" if sys.platform == "win32" else "/"

            # Folders
            # Data
            folderTmpData = Config.getImagePathConfig(["tmp"])[0]
            ImageUtil.createFolder(folderTmpData)
            # Labels
            folderTmpLabel = "tmp_dpnbrepnponbprz"
            ImageUtil.createFolder(folderTmpLabel)
            # ImPath
            folderTmpImPath = "tmp_rbponrbponbnr"
            ImageUtil.createFolder(folderTmpImPath)
            

            index = 0
            start = datetime.now()
            for filename in files:
                if index % 100 == 0:
                    print("GetDataCannyAndLabels")
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                # CSVs
                # data
                csv_data_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_data.csv"
                csv_data_path = os.path.join(folderTmpData, csv_data_name)
                # label
                csv_label_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_label.csv"
                csv_label_path = os.path.join(folderTmpLabel, csv_label_name)
                # data_image_path
                csv_impath_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_impath.csv"
                csv_impath_path = os.path.join(folderTmpImPath, csv_impath_name)

                if os.path.isfile(csv_data_path) and os.path.isfile(csv_label_path) and os.path.isfile(csv_impath_path):
                    continue

                # init
                data_image_path = []
                labels = []
                data_image = []

                im_array = ndimage.imread(filename)

                width = im_array.shape[0]
                height = im_array.shape[1]

                for x in range(width):
                    for y in range(height):
                        pixel = im_array[x][y]

                        data_image.append(pixel)
                
                # percentage
                nb = np.where( np.array(data_image) == 0)
                percentage = nb[0].shape[0] / len(data_image) * 100

                if percentage <= 97:
                    personne_name = DataExtractor.getPersonneName(filename)
                    label = names.index(personne_name)
                    labels.append(label) # labels start (one unsigned byte each)

                    data_image_path.append( filename )

                    # Write CSVs
                    # Image
                    DataExtractor.writeCSVFile(csv_data_path, data_image)
                    # Label
                    DataExtractor.writeCSVFile(csv_label_path, labels)
                    # ImPath
                    DataExtractor.writeCSVFile(csv_impath_path, data_image_path)

            # Concat
            csv_order = "read_file_order.csv"
            ImageUtil.deleteFile(csv_order)
            # Data
            data = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_data, csv_order, folderTmpData, "_data.csv" )
            # Label
            labels = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_labels, csv_order, folderTmpLabel, "_label.csv" )
            # ImPath
            impath_array = DataExtractor.ConcatenationDataCSV(filename_csv_impath, np.str, folder=folderTmpImPath )

        # Shape
        imageSize = 300*300 # todo use config

        number = int(data.shape[0] / imageSize )
        shapeData = [ number, imageSize]
        shapeLabels = [ number ]
        
        # Reshape
        data = np.reshape(data, shapeData)
        labels = np.reshape(labels, shapeLabels)

        # Clear
        if not fileExist:
            ImageUtil.clearFolder(folderTmpData)
            ImageUtil.clearFolder(folderTmpLabel)
            ImageUtil.clearFolder(folderTmpImPath)
            ImageUtil.deleteFile(csv_order)
            ImageUtil.deleteFile( os.path.join("files", "tmp_dpnbrepnponbprz_files.csv"))
            ImageUtil.deleteFile( os.path.join("files", "tmp_lnbrnbpnvpenvnrpvbzs_files.csv"))
            ImageUtil.deleteFile( os.path.join("files", "tmp_rbponrbponbnr_files.csv"))

        return data, labels, impath_array

    # todo make comment
    # Todo changer le param Files en Folder
    @staticmethod
    def getDataScharrAndLabels(filename_memmap_base, files, dtype, doNormalize=False, doThreshold=False):
        # todo supprimer les param inutile
        print("## GetDataScharrAndLabels")
        if doThreshold:
            doNormalize = False

        # Files
        filename_memmap_data = filename_memmap_base + "_data.dat"
        filename_memmap_labels = filename_memmap_base + "_labels.dat"
        filename_csv_impath = filename_memmap_base + "_impath.csv"

        fileExist = os.path.isfile(filename_memmap_data) and os.path.isfile(filename_memmap_labels) and os.path.isfile(filename_csv_impath)
        
        print("fileExist : ", fileExist)
        if fileExist:
            # Data
            data = DataExtractor.readMemmap(filename_memmap_data, dtype)
            # Label
            labels = DataExtractor.readMemmap(filename_memmap_labels, np.int16)
            # ImPath
            impath_array = DataExtractor.readCSVFile(filename_csv_impath, np.str)
        else:
            # Clear
            # todo utiliser util
            ImageUtil.deleteFile( filename_memmap_data )
            ImageUtil.deleteFile( filename_memmap_labels )
            ImageUtil.deleteFile( filename_csv_impath )
            
            # generate names
            names = DataExtractor.getNames(files)

            separator = "\\" if sys.platform == "win32" else "/" # todo utiliser separator global

            # Folders
            # todo utiliser config
            # Data
            # folderTmpData = Config.getImagePathConfig(["tmp"])[0]
            config = Config()
            folderTmpData = config.image.path.tmp
            ImageUtil.createFolder(folderTmpData) # todo utiliser Util
            # Labels
            folderTmpLabel = "tmp_dpnbrepnponbprz" # todo utiliser conf
            ImageUtil.createFolder(folderTmpLabel) # todo utiliser Util
            # ImPath
            folderTmpImPath = "tmp_rbponrbponbnr" # todo utiliser conf
            ImageUtil.createFolder(folderTmpImPath) # todo utilsier Util
            

            # variable de debug
            index = 0
            start = datetime.now()
            for filename in files:
                if index % 100 == 0:
                    # todo mettre des logs en place
                    print("getDataScharrAndLabel")
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                # CSVs
                # data
                # todo "_data.csv" --> magic string --> todo utiliser config
                csv_data_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_data.csv"
                csv_data_path = os.path.join(folderTmpData, csv_data_name)
                
                # label
                # todo "_label.csv" --> magic string --> todo utiliser config
                csv_label_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_label.csv"
                csv_label_path = os.path.join(folderTmpLabel, csv_label_name)
                
                # data_image_path
                # todo "_impath.csv" --> magic string --> todo utiliser config
                csv_impath_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_impath.csv"
                csv_impath_path = os.path.join(folderTmpImPath, csv_impath_name)

                # Si les fichiers existe on passe au suivant
                if os.path.isfile(csv_data_path) and os.path.isfile(csv_label_path) and os.path.isfile(csv_impath_path):
                    continue

                # init
                data_image_path = []
                labels = []
                data_image = []

                im_array = ndimage.imread(filename)

                width = im_array.shape[0]
                height = im_array.shape[1]
                deep = im_array.shape[2]

                # Supprimer les DoNormalise et Else qui ne servent plus
                if doNormalize:
                    for x in range(width):
                        for y in range(height):
                            a = 0
                            b = 0
                            c = 0
                            for z in range(deep):
                                pixel = im_array[x][y][z]
                                
                                if z == 0:
                                    a = pixel
                                elif z == 1:
                                    b = pixel
                                else:
                                    c = pixel
                                
                            norm = round( ( (int(a) + int(b) + int(c)) / 765.0) , 3) # 3 * 255

                            data_image.append(norm)
                elif doThreshold:
                    for x in range(width):
                        for y in range(height):
                            for z in range(deep):
                                pixel = im_array[x][y][z]
                                
                                if pixel > 10:
                                    value = 1
                                else:
                                    value = 0
                                
                                data_image.append(value)
                else:
                    for x in range(width):
                        for y in range(height):
                            for z in range(deep):
                                pixel = im_array[x][y][z]
                                
                                data_image.append(pixel)
                # percentage
                nb = np.where( np.array(data_image) == 0 )
                percentage = nb[0].shape[0] / len(data_image) * 100

                if percentage <= 80: # todo magic number --> todo utiliser conf
                    personne_name = DataExtractor.getPersonneName(filename)
                    label = names.index(personne_name)
                    labels.append(label) # labels start (one unsigned byte each)

                    data_image_path.append( filename )

                    # Write CSVs
                    # Image
                    DataExtractor.writeCSVFile(csv_data_path, data_image)
                    # Label
                    DataExtractor.writeCSVFile(csv_label_path, labels)
                    # ImPath
                    DataExtractor.writeCSVFile(csv_impath_path, data_image_path)
            
            # Concat
            csv_order = os.path.join("files", "read_file_order.csv") # todo utiliser config
            ImageUtil.deleteFile(csv_order)

            # todo trouver un meilleur algoithme --> Long --> et pas util de passer par des csv ?
            # Data
            data = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_data, csv_order, folderTmpData, "_data.csv", dtype )
            # Label
            labels = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_labels, csv_order, folderTmpLabel, "_label.csv", np.int16 )
            # ImPath
            impath_array = DataExtractor.ConcatenationDataCSVOrder(filename_csv_impath, csv_order, folderTmpImPath, "_impath.csv", np.str )

        # Shape
        imageSize = 300*300 # todo use config
        if not doNormalize: # todo doNormalize doit etre supprimer
            imageSize *= 3
        
        number = int(data.shape[0] / imageSize )
        shapeData = [ number, imageSize]
        shapeLabels = [ number ]
        
        # Reshape
        data = np.reshape(data, shapeData)
        labels = np.reshape(labels, shapeLabels)

        # Clear
        if not fileExist:
            ImageUtil.clearFolder(folderTmpData)
            ImageUtil.clearFolder(folderTmpLabel)
            ImageUtil.clearFolder(folderTmpImPath)
            ImageUtil.deleteFile(csv_order)
            ImageUtil.deleteFile( os.path.join("files", "tmp_dpnbrepnponbprz_files.csv") )
            ImageUtil.deleteFile( os.path.join("files", "tmp_lnbrnbpnvpenvnrpvbzs_files.csv") )
            ImageUtil.deleteFile( os.path.join("files", "tmp_rbponrbponbnr_files.csv") )

        return data, labels, impath_array

    # todo make comment
    @staticmethod
    def getDataRGB(filename_memmap, files):
        # return la lecture du CSV (plus rapide)
        if os.path.isfile(filename_memmap):
            data = DataExtractor.readCSVFile(filename_memmap)
        else:
            separator = "\\" if sys.platform == "win32" else "/"

            # Folder
            folderTmp = Config.getImagePathConfig(["tmp"])[0]
            ImageUtil.clearAndCreatFolder(folderTmp)

            # fetch data
            index = 0 # pour debug
            for filename in files:
                data_image = []
                
                if index % 100 == 0:
                    print("getDataRGB")
                    print("index : ", index)
                index += 1

                # csv_file
                csv_file_name = filename[filename.rfind(separator)+1:] + ".csv"
                csv_file_path = os.path.join(folderTmp, csv_file_name)

                if os.path.isfile(csv_file_path):
                    return DataExtractor.readCSVFile(csv_file_path)


                im_array = ndimage.imread(filename, mode='RGB')

                width = im_array.shape[0]
                height = im_array.shape[1]
                deep = im_array.shape[2] # channels

                for x in range(width):
                    for y in range(height):
                        for z in range(deep):
                            pixel = im_array[x][y][z]
                            
                            data_image.append(pixel)

                data_image = np.array(data_image)

                # write cvs
                DataExtractor.writeCSVFile(csv_file_path, data_image)

    
            data = DataExtractor.ConcatenationDataMemMap(filename_memmap, folder=folderTmp )

            # delete file
            ImageUtil.clearFolder(folderTmp)

        return data

    # todo make comment
    @staticmethod
    def getDataHexa(filename_memmap, files):
        # return la lecture du memmap (plus rapide)
        if os.path.isfile(filename_memmap):
            data = DataExtractor.readMemmap(filename_memmap)
        else:
            separator = "\\" if sys.platform == "win32" else "/"

            # Folder
            folderTmp = Config.getImagePathConfig(["tmp"])[0]
            ImageUtil.createFolder(folderTmp)

            # fetch Data

            index = 0 # pour debug
            start = datetime.now()
            for filename in files:
                if index % 100 == 0:
                    print("getDataHexa")
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()

                index += 1 

                # CSV
                csv_file_name = filename[filename.rfind(separator)+1:] + ".csv"
                csv_file_path = os.path.join(folderTmp, csv_file_name)

                if os.path.isfile(csv_file_path):
                    continue

                data_image = []
                im_array = ndimage.imread(filename, mode='RGB')

                width = im_array.shape[0]
                height = im_array.shape[1]
                deep = im_array.shape[2]

                for x in range(width):
                    for y in range(height):
                        r = -1
                        g = -1
                        b = -1
                        for z in range(deep):
                            pixel = im_array[x][y][z]

                            if z == 0:
                                r = pixel
                            elif z == 1:
                                g = pixel
                            elif z == 2:
                                b = pixel

                        if r == -1 or g == -1 or b == -1:
                            raise ValueError('Error color not found r:%d, g:%d, b%d' % (r,g,b))

                        # Hexa color
                        color = "0x" + (hex(r) + hex(g) + hex(b)).replace("0x","")
                        # convert to base 10
                        integerValue = int(color, 16)
                        
                        data_image.append(integerValue)

                # write cvs
                DataExtractor.writeCSVFile(csv_file_path, data_image)

            
            data = DataExtractor.ConcatenationDataMemMap(filename_memmap, folder=folderTmp )

        # Shape
        imageSize = 300*300 # todo use config

        shape = [ int(data.shape[0] / imageSize ), imageSize]
        data = np.reshape(data, shape)

        return data

    # todo make comment
    @staticmethod
    def getDataGrey(filename_memmap, files, dtype, doNorme=False):
        # return la lecture du memmap (plus rapide)
        if os.path.isfile(filename_memmap):
            data = DataExtractor.readMemmap(filename_memmap, dtype)
        else:
            separator = "\\" if sys.platform == "win32" else "/"

            # Folder
            folderTmp = Config.getImagePathConfig(["tmp"])[0]
            ImageUtil.createFolder(folderTmp)

            # fetch Data
            index = 0 # pour debug
            for filename in files:
                if index % 100 == 0:
                    print("getDataGrey")
                    print("index : ", index)
                index += 1 

                # CSV
                csv_file_name = filename[filename.rfind(separator)+1:] + ".csv"
                csv_file_path = os.path.join(folderTmp, csv_file_name)

                if os.path.isfile(csv_file_path):
                    continue

                data_image = []
                im_array = ndimage.imread(filename)
                
                width = im_array.shape[0]
                height = im_array.shape[1]

                for x in range(width):
                    for y in range(height):
                        pixel = im_array[x][y]

                        if doNorme:
                            pixel = round( (pixel / 255) ** 2, 2)
                        
                        data_image.append(pixel)

                # write cvs
                DataExtractor.writeCSVFile(csv_file_path, data_image)

            
            data = DataExtractor.ConcatenationDataMemMap(filename_memmap, np.float32, folder=folderTmp)

            # todo Delete tmpFolder
            ImageUtil.clearFolder(folderTmp)

        # Shape
        imageSize = 300*300 # todo use config

        shape = [ int(data.shape[0] / imageSize ), imageSize]
        data = np.reshape(data, shape)

        return data

     # todo make comment
    
    # todo make comment
    @staticmethod
    def getDataSobelAndLabel(filename_memmap_base, files):
        # Files
        filename_memmap_data = filename_memmap_base + "_data.dat"
        filename_memmap_labels = filename_memmap_base + "_labels.dat"
        filename_csv_impath = filename_memmap_base + "_impath.csv"

        fileExist = os.path.isfile(filename_memmap_data) and os.path.isfile(filename_memmap_labels) and os.path.isfile(filename_csv_impath)

        if fileExist:
            # Data
            data = DataExtractor.readMemmap(filename_memmap_data)
            # Label
            labels = DataExtractor.readMemmap(filename_memmap_labels)
            # ImPath
            impath_array = DataExtractor.readCSVFile(filename_csv_impath)
        else:
            # generate names
            names = DataExtractor.getNames(files)

            separator = "\\" if sys.platform == "win32" else "/"

            # Folders
            # Data
            folderTmpData = Config.getImagePathConfig(["tmp"])[0]
            ImageUtil.createFolder(folderTmpData)
            # Labels
            folderTmpLabel = "tmp_dpnbrepnponbprz"
            ImageUtil.createFolder(folderTmpLabel)
            # ImPath
            folderTmpImPath = "tmp_rbponrbponbnr"
            ImageUtil.createFolder(folderTmpImPath)
            

            index = 0
            start = datetime.now()
            for filename in files:
                if index % 100 == 0:
                    print("GetDataSobelAndLabel")
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                # CSVs
                # data
                csv_data_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_data.csv"
                csv_data_path = os.path.join(folderTmpData, csv_data_name)
                # label
                csv_label_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_label.csv"
                csv_label_path = os.path.join(folderTmpLabel, csv_label_name)
                # data_image_path
                csv_impath_name = filename[filename.rfind(separator)+1:filename.rfind(".")] + "_impath.csv"
                csv_impath_path = os.path.join(folderTmpImPath, csv_impath_name)

                if os.path.isfile(csv_data_path) and os.path.isfile(csv_label_path) and os.path.isfile(csv_impath_path):
                    continue

                # init
                data_image_path = []
                labels = []
                data_image = []

                im_array = ndimage.imread(filename)

                width = im_array.shape[0]
                height = im_array.shape[1]

                for x in range(width):
                    for y in range(height):
                        pixel = im_array[x][y]

                        data_image.append(pixel)
                
                # percentage
                nb = np.where( np.array(data_image) == 0)
                percentage = nb[0].shape[0] / len(data_image) * 100

                if percentage <= 80:
                    personne_name = DataExtractor.getPersonneName(filename)
                    label = names.index(personne_name)
                    labels.append(label) # labels start (one unsigned byte each)

                    data_image_path.append( filename )

                    # Write CSVs
                    # Image
                    DataExtractor.writeCSVFile(csv_data_path, data_image)
                    # Label
                    DataExtractor.writeCSVFile(csv_label_path, labels)
                    # ImPath
                    DataExtractor.writeCSVFile(csv_impath_path, data_image_path)

            # Concat
            csv_order = "read_file_order.csv"
            ImageUtil.deleteFile(csv_order)
            # Data
            data = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_data, csv_order, folderTmpData, "_data.csv" )
            # Label
            labels = DataExtractor.ConcatenationDataMemMapOrder(filename_memmap_labels, csv_order, folderTmpLabel, "_label.csv" )
            # ImPath
            impath_array = DataExtractor.ConcatenationDataCSV(filename_csv_impath, folder=folderTmpImPath )

        # Shape
        imageSize = 300*300 # todo use config

        number = int(data.shape[0] / imageSize )
        shapeData = [ number, imageSize]
        shapeLabels = [ number ]
        
        # Reshape
        data = np.reshape(data, shapeData)
        labels = np.reshape(labels, shapeLabels)

        # Clear
        if not fileExist:
            ImageUtil.clearFolder(folderTmpData)
            ImageUtil.clearFolder(folderTmpLabel)
            ImageUtil.clearFolder(folderTmpImPath)
            ImageUtil.deleteFile(csv_order)
            ImageUtil.deleteFile(os.path.join("files", "tmp_dpnbrepnponbprz_files.csv"))
            ImageUtil.deleteFile(os.path.join("files", "tmp_lnbrnbpnvpenvnrpvbzs_files.csv"))
            ImageUtil.deleteFile(os.path.join("files", "tmp_rbponrbponbnr_files.csv"))

        return data, labels, impath_array
    
    # todo make comment
    @staticmethod
    def ConcatenationDataMemMap(filename, dtype, files=None, folder=None):
        print("Start ConcatenationDataMemMap")

        if not os.path.isfile(filename):
            if files == None and folder == None:
                raise ValueError("DataExtractor.ConcatenationData expected files or folder param")

            if files == None:
                files = ImageUtil.getAllFiles(folder)

            # write memmap
            index = 0 # juste pour dev
            start = datetime.now()
            for file in files:
                if index % 100 == 0:
                    print("##### ConcatenationData() filename : ", filename)
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                data = DataExtractor.readCSVFile(file, dtype)

                DataExtractor.writeMemMap(filename, data, dtype)

        # READ csv
        return DataExtractor.readMemmap(filename, dtype)

    # todo make comment
    @staticmethod
    def ConcatenationDataMemMapOrder(filename, csv_order, folder, endFile, dtype):
        print("Start ConcatenationdataMemMapOrder")
        
        if not os.path.isfile(filename):
            # Order
            if os.path.isfile(csv_order):
                order = DataExtractor.readCSVFile(csv_order, np.str)
            else:
                order = ImageUtil.getAllFiles(folder)
                DataExtractor.writeCSVFile(csv_order, order)

            separator = "\\" if sys.platform == "win32" else "/"
            
            # read
            index = 0
            start = datetime.now()
            for file in order:
                if index % 100 == 0:
                    print("### ConcatenationDataMemMapOrder filename : ", filename)
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                # Name File
                file_name = file[file.rfind(separator)+1:file.rfind("_")] + endFile
                file_path = os.path.join(folder, file_name)

                # Get Data
                data = DataExtractor.readCSVFile(file_path, dtype)

                # Write
                DataExtractor.writeMemMap(filename, data, dtype)

                # TEST
                data_written = DataExtractor.readMemmap(filename, dtype)
        
        return DataExtractor.readMemmap(filename, dtype)

    # todo make comment
    @staticmethod
    def ConcatenationDataCSVOrder(filename, csv_order, folder, endFile, dtype):
        print("Start ConcatenationDataCSVOrder")

        if not os.path.isfile(filename):
            # Order 
            if os.path.isfile(csv_order):
                order = DataExtractor.readCSVFile(csv_order, np.str)
            else:
                order = ImageUtil.getAllFiles(folder)
                DataExtractor.writeCSVFile(csv_order, order)

            separator = "\\" if sys.platform == "win32" else "/"

            # write csv
            index = 0 # juste pour dev
            start = datetime.now()
            for im_path in order:
                if index % 100 == 0:
                    print("##### ConcatenationData() filename : ", filename)
                    print("index : ", index)
                    print("time : ", datetime.now()-start)
                    start = datetime.now()
                index += 1

                # Name File
                file_name = im_path[im_path.rfind(separator)+1:im_path.rfind("_")] + endFile
                file_path = os.path.join(folder, file_name)

                data = DataExtractor.readCSVFile(file_path, dtype)

                DataExtractor.writeCSVFile(filename, data)

        # READ csv
        return DataExtractor.readCSVFile(filename, dtype)

    # todo make comment
    @staticmethod
    def ConcatenationDataCSV(csv_path, dtype, files=None, folder=None):
        print("Start ConcatenationDataCSV")

        if not os.path.isfile(csv_path):
            if files == None and folder == None:
                raise ValueError("DataExtractor.ConcatenationData expected files or folder param")

            if files == None:
                files = ImageUtil.getAllFiles(folder)

            # write csv
            index = 0 # juste pour dev
            for file in files:
                if index % 100 == 0:
                    print("##### ConcatenationData() csv_path : ", csv_path)
                    print("index : ", index)
                index += 1
                data = DataExtractor.readCSVFile(file, dtype)

                DataExtractor.writeCSVFile(csv_path, data)

        # READ csv
        return DataExtractor.readCSVFile(csv_path, dtype)

    # todo make comment
    @staticmethod
    def DataLabelFormat(labels):
        return np.array(labels, dtype=np.uint8)

    # todo make comment
    @staticmethod
    def DataImageFormat(images):
        # Formatage des ndarrays pour le return
        # Images
        fileConfig = Config.getImageConfig(["height", "width", "channels"])
        heightWidthDeep = fileConfig[0]*fileConfig[1]*fileConfig[2]

        shape = [ int(len(images)/heightWidthDeep) , (heightWidthDeep)]

        data_image = np.array(images, dtype=np.float32)
        data_image = np.reshape(data_image, shape)

        return data_image

    # todo make comment
    @staticmethod
    def getShape(images):
        fileConfig = Config.getImageConfig(["height", "width", "channels"])
        heightWidthDeep = fileConfig[0]*fileConfig[1]*fileConfig[2]
        #print("len(images) : ", len(images))
        #print("heightWidthDeep : ", heightWidthDeep)
        shape = [ len(images) , (heightWidthDeep)]

        return shape

    # todo make comment
    # todo deplacé cette fonction dans un Util dédié
    @staticmethod
    def writeCSVFile(csv_path, data):
        ofile = open(csv_path, "a")

        data = np.array(data)

        if len(data.shape) != 2:
            writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar='\\')
            writer.writerow(data)
        else:
            writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar='\\')
            for index in range( int(data.size / data.shape[1]) ):
                if index == 0:
                    debut = index
                    fin = data.shape[1]
                else:
                    debut = index * data.shape[1]
                    fin = debut + data.shape[1]

                row = data.reshape(-1)[ debut : fin ]
                writer.writerow(row)
        
        ofile.close()

    # todo make comment
    # todo deplacé cette fonction dans un Util dédié
    @staticmethod
    def readCSVFile(csv_file, dtype):
        if not os.path.isfile(csv_file):
            raise ValueError("file doesn't exist file : ", csv_file)
        
        # open file
        ofile  = open(csv_file, "r")
        reader = csv.reader(ofile)

        # Get Data
        data = np.array([])
        
        nb_row = 0
        for row in reader:
            if len(row) > 0:
                nb_row += 1
                data = np.append(data, row).astype(dtype)

                shape = len(row)

        # Shape
        if nb_row > 1:
            shape = [ nb_row, shape ]

            # convert to ndArray + reshape 
            data = np.array(data).reshape(shape).astype(dtype)
        
        # close file
        ofile.close()

        return data

    # todo make comment
    # todo deplacé cette fonction dans un Util dédié
    @staticmethod
    def readMemmap(filename, dtype):
        fpo = np.memmap(filename, dtype=dtype, mode='r')

        return fpo

    # todo make comment
    # shape format : shape=(x,y)
    # todo deplacé cette fonction dans un Util dédié
    @staticmethod
    def writeMemMap(filename, data, dtype ):
        if type(data) != np.ndarray:
            data = np.array(data)

        # shape
        shape = ()
        total = 1
        index = 0
        for element in data.shape:
            shape += (element,)

            if index > 0:
                total *= element

            index +=1

        # Get Data
        if os.path.isfile(filename):

            oldData = DataExtractor.readMemmap(filename, dtype)
            oldSize = int(oldData.shape[0] / total)
            
            size = oldSize + data.shape[0]
            
            # TMP
            tmp_file = "aaaaainvzns.dat"
            tmp = np.memmap(tmp_file, mode='w+', shape=shape, dtype=dtype)
            tmp = data

            shape = ()
            shape += (size,)
            for i in range(1,len(data.shape)):
                shape += (data.shape[i],)

            fp = np.memmap(filename, mode='r+', shape=shape, dtype=dtype)
            if len(data.shape) == 1:
                fp[oldSize:] = tmp
            else:
                fp[oldSize:,:] = tmp

            os.remove(tmp_file)
        else: 
            fp = np.memmap(filename, mode='w+', shape=shape, dtype=dtype)

            for i in range(data.shape[0]):
                fp[i] = data[i]

