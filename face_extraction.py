import os, cv2

from image_util import ImageUtil
from config import Config

class FaceExtraction:
    # # Doc : https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html?highlight=videocapture#cv2.VideoCapture.get

    def __init__(self):

        # Initialisation
        self.videos_folder = "videos" # todo utilisé config --> doit etre la meme conf que dl_videos.py 
        self.images_folder = "tmp" # todo utilsé config
        self.images_proceed = "images" # todo utilisé config
        # todo passer le temps en milliseconde --> calcule effectuer dans _video_time
        self.NUMBER_SECOND_TO_ADD = 5 # todo magic number dans config 
        self.config = Config()

        self._initialisation()

    def _initialisation(self):
        ImageUtil.clearAndCreatFolder( self.images_folder ) # todo change ImageUtil par Util
        
        # Recuperation des fichiers
        videos = ImageUtil.getAllFiles( self.videos_folder ) # todo change ImageUtil par Util

        # Recuperation des images des videos
        count = 0
        for video in videos:
            # todo mettre des logs en place
            print("video : ", video)

            # RECUPERATION DES IMAGES DANS LES VIDEOS + Extraction de visage
            count += 1
            currentVideoTime = 0
            frameRate = 0
            nbTotalFrame = 0
            videoLenght = 0
            numImage = 0

            # cascPath = os.path.join("requirement", "haarcascade_frontalface_default.xml")
            # cascPath = os.path.join( self.config.face_detection.folder, self.config.face_detection.file )
            # faceCascade = cv2.CascadeClassifier(cascPath)

            # Voir Documentation (cf en haut du fichier)
            self.video_capture = cv2.VideoCapture(video)

            if self.video_capture.isOpened():
                nbTotalFrame = self.video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
                frameRate = self.video_capture.get(cv2.CAP_PROP_FPS)
                #print("frameRate : ", frameRate)

                #print("nombre total de frame dans la video : ", nbTotalFrame)
                videoLenght = (nbTotalFrame/frameRate)

            #print("video length : ", videoLenght, "s")

            while (videoLenght * 1000) > currentVideoTime: # *1000 --> conversion en miliseconde
                if not self.video_capture.isOpened():
                    print('Unable to load camera.')
                    time.sleep(5)
                    pass

                # Capture frame-by-frame
                ret, frame = self.video_capture.read()

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

                # si une erreur survient sur une image, on passe a la suivante
                if not ret:
                    # todo mettre des logs en place
                    print("Une erreur est surevenu sur l'image")
                    print("video : ", video)
                    print("temps : ", self.video_capture.get(cv2.CAP_PROP_POS_MSEC))
                    print("passage a l'image suivante")
                    currentVideoTime = self._videoTime()
                    continue
                
                # save image
                video_name = video[video.rfind("/")+1:]
                # extension obligatoire sinon openCV ne sait pas enregister l'image
                # todo extension doit etre dans les config
                fileName = video_name[:video_name.rfind(".")] + "_img" + str(numImage) + ".png"
                output = os.path.join(self.images_folder, fileName.lower())
                
                cv2.imwrite(output, frame)
                numImage = numImage + 1

                # Passage a l'image suivante
                currentVideoTime = self._videoTime()
        
        # Face Detection
        ImageUtil.formatImages(self.images_folder, self.images_proceed, doConvert=False)

        # Clean
        ImageUtil.clearFolder(self.images_folder) # todo utiliser Util

        
    def _videoTime(self):
        currentVideoTime = self.video_capture.get(cv2.CAP_PROP_POS_MSEC)
        nextVideoTime = currentVideoTime + (self.NUMBER_SECOND_TO_ADD * 1000)

        self.video_capture.set(cv2.CAP_PROP_POS_MSEC, nextVideoTime )
        return nextVideoTime

