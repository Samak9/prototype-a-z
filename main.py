import os, sys, argparse
from shutil import copyfile

from dl_videos import DownloadVideo
from face_extraction import FaceExtraction
from image_util import ImageUtil
from data_extractor import DataExtractor
from fetch_data import FetchData
from clusters import Clusters
from util import Util

# Parser
parser = argparse.ArgumentParser(description='mise en place des clusters', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

# Arguments
parser.add_argument('--file_name', default='example/example.txt', type=str)
parser.add_argument('--clusters_n', default=3, type=int)

args = parser.parse_args()



# 1. Récuperation du(des) vidéo(s)  
login = "sylvain.noulet@speach.me"
password = "ikfoXyUT"
DownloadVideo(login, password)


# 2. Extraction d'image  
# 3. Fitrer les images.
#     * Garder uniquement les visages
#     * Croper les images
#     * Redimensionner les images (test effectué en 300x300)
#     * Mettre la dimension ".png" (autre dimension non testé)
FaceExtraction()


# 4. Passer un scharr algorithme sur les images
# 5. Effectuer un treshold sur le scharr algorithme
image_folder = "images_scharr" # todo utiliser config

if not os.path.isdir(image_folder):
    image_init = "images" # todo utiliser config
    ImageUtil.convertImageToScharr(image_init, image_folder)

# 6. Limiter le nombre d'élément dans la RAM (maxi tester 50 * 25 * 300 * 300 * 3)  
#     | Nombre | Description|
#     | :-----:|:-----------|
#     | 50     | Un nombre magique                  |
#     | 25     | Nombre de personnes lors du test   |
#     | 300    | Hauteur de chaque image            |
#     | 300    | Largeur de chaque image            |
#     | 3      | Nombre de channels de chaque image |
# 7. Application du TSNE algorithm
data, labels, names, files = FetchData(args.file_name).initialize().getValues()


# 8. Application du AHC algorithm. *Cette algorithme a été écrit en interne*

clusters_n = args.clusters_n

clusters = Clusters(data, files)

print("Création des clusters")
print( "Il y a " + str(clusters_n) + " cluster(s) attendu" )
index = 1
while len(clusters.clusters) > clusters_n and not clusters.enough_cluster:
    clusters.fusion_clusters(index)
    index += 1
clusters.graph.save_graph()

# todo mettre ça dans clusters
# Save clusters
clusters_array = Util.get_all_clusters_to_string( clusters.clusters )

filename = os.path.join("files", "clusters.json")
Util.deleteFile(filename)

with open(filename, 'w') as file:
    file.write( json.dumps( clusters_array ) ) # use `json.loads` to do the reverse


# 9. Répartission des images selon les clusters crée par l'agorithm <small>(*précédant --> AHC*)</small>
# Clusters
print("Debut de la répartion par cluster")
folder = "clusters" # todo utiliser config

# JUST POUR DEV
Util.clearFolder(folder)
# FIN JUST POUR DEV

Util.createFolder( folder )

separator = "\\" if sys.platform == "win32" else "/"

for cluster in clusters.clusters:
    # Folder
    name_cluster = str(cluster["id"])
    cluster_folder = os.path.join(folder, name_cluster)
    Util.createFolder( cluster_folder )

    # files
    for file_path in cluster["files"]:
        filename = file_path.split(separator)[-1]
        destination = os.path.join(cluster_folder, filename)

        copyfile( file_path, destination )  

# 10. Si des images dans les clusters sont connu alors **Labelisation** des clusters 
# 11. Si une nouvelle vidéo est reçu procédé aux étapes [1, 2, 3, 4, 5] et si les images sont trouvé dans un cluster alors on connait la personne a l'intérrieur