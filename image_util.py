import os, sys, csv, cv2, random
import numpy as np
from datetime import datetime

from config import Config

class ImageUtil:
    # Supprimer les Fonctions non utilisé
    # Supprimer les fonctions deleteFile, clearFolder, createFolder, clearAndCreatFolder et utilsé celles de Util a la place

    # delete file
    @staticmethod
    def deleteFile(file):
        if os.path.isfile(file):
            os.remove(file)

    # clear
    @staticmethod
    def clearFolder(directory):
        if os.path.exists(directory):
            for dirPath, dirNames, files in os.walk(directory):
                for file in files:
                    filePath = os.path.join(dirPath, file)
                    #fd = os.open( file, os.O_RDWR|os.O_CREAT )
                    #os.close( fd )

                    os.remove(filePath)

                for dirName in dirNames:
                    ImageUtil.clearFolder( os.path.join(dirPath, dirName) )

            os.rmdir(directory)

    # createFolder
    # verifie l'existance du dossier test si non le crée
    @staticmethod
    def createFolder(folder):
        if not os.path.exists(folder):
            os.makedirs(folder)

    # todo make comment
    @staticmethod
    def clearAndCreatFolder(folder):
        ImageUtil.clearFolder(folder)
        ImageUtil.createFolder(folder)

    # todo make comment
    @staticmethod
    def convertImage(input,output):
        # todo sert a rien de garder le code concernant Canny algorithm

        print("ConvertImage")

        config = Config()

        # todo Duplicata avec data_extractor getData()
        # Canny
        # isCanny = Config.getImageConfig(["colored"])[0] == "Canny"
        isCanny = config.image.colored == "Canny"
        # Scharr
        # isScharr = Config.getImageConfig(["colored"])[0] == "Scharr"
        isScharr = config.image.colored == "Scharr"

        if isCanny:
            ImageUtil.convertImageToCanny(input, output)
        elif isScharr:
            ImageUtil.convertImageToScharr(input, output)
        else:
            raise ValueError("DataExtractor pas dev")


    # todo make comment
    @staticmethod
    def convertImageToCanny(input, output):
        print("ConvertImageToCanny")
        separator = "\\" if sys.platform == "win32" else "/"

        # init
        ImageUtil.createFolder(output)

        # files
        files = ImageUtil.getAllFiles(input)

        for file in files:
            imgRead = cv2.imread(file)
            imgRead = cv2.Canny(imgRead, 40,70)

            filename = file.split(separator)[-1]
            destinationFile = os.path.join(output,filename)
            cv2.imwrite(destinationFile, imgRead)

    # todo make comment
    @staticmethod
    def convertImageToBlur(input, output):
        print("ConvertImageToBlur")
        separator = "\\" if sys.platform == "win32" else "/"

        # init
        ImageUtil.createFolder(output)

        # files
        files = ImageUtil.getAllFiles(input)

        for file in files:
            imgRead = cv2.imread(file)
            blur = cv2.blur(imgRead, (3, 3) )

            filename = file.split(separator)[-1]
            destinationFile = os.path.join(output,filename)
            cv2.imwrite(destinationFile, imgRead)

    # todo make comment
    @staticmethod
    def convertImageToGrey(input, output):
        print("ConvertImageToGrey")
        separator = "\\" if sys.platform == "win32" else "/"

        # init
        ImageUtil.createFolder(output)

        # files
        files = ImageUtil.getAllFiles(input)

        for file in files:
            imgRead = cv2.imread(file)
            imgRead = cv2.cvtColor(imgRead, cv2.COLOR_BGR2GRAY)

            filename = file.split(separator)[-1]
            destinationFile = os.path.join(output,filename)
            cv2.imwrite(destinationFile, imgRead)

     # todo make comment
   
    @staticmethod
    def convertImageToScharr(input,output):
        print("convertImageToScharr")
        separator = "\\" if sys.platform == "win32" else "/" # todo Crée un separator global (voir dans un Util ?)

        # init
        ImageUtil.createFolder(output) # todo utiliser Util

        # files
        files = ImageUtil.getAllFiles(input) # todo Pourquoi getAllFiles dans ImageUtil ?


        nbTotalImage = len(files)
        index = 0
        start = datetime.now()
        for file in files:
            # todo rename file en un mot moins generique
            if index % 1000 == 0:
                # todo garder ça ? mettre en place des logs ?
                print("convertImageToScharr")
                print("index : ", index)
                print("time: ", (datetime.now()-start))
                start = datetime.now()
            index += 1
            
            # Read image
            img = cv2.imread(file).astype(np.float64)
            imgRead = cv2.Scharr(img, -1, 0, 1) # todo utilsé conf pour les magics number
            
            # Destination file
            filename = file.split(separator)[-1]
            destinationFile = os.path.join(output,filename)
            
            # Write image
            cv2.imwrite(destinationFile, imgRead)

    # todo make comment
    @staticmethod
    def convertImageToSobel(input, output):
        print("convertImageToSobel")
        separator = "\\" if sys.platform == "win32" else "/"

        # init
        ImageUtil.createFolder(output)

        # files
        files = ImageUtil.getAllFiles(input)

        for file in files:
            img = cv2.imread(file,0)
            sobel = cv2.Sobel(img, cv2.CV_32F, 2, 2, ksize=7)

            filename = file.split(separator)[-1]
            destinationFile = os.path.join(output,filename)

            cv2.imwrite(destinationFile, sobel)

    # Met les images dans le format attendu
    # GrayScale, Crop, Resize, Convertion en JPG
    @staticmethod
    def formatImages(folderInput, folderOutput, doConvert=True):
        # todo supprimer l'option doConvert
        # folderTmp = Config.getImagePathConfig(["tmp"])[0]
        config = Config()
        folderTmp = config.image.path.tmp # todo renomer cette variable
        folderTmp2 = "tmp_klrdpvrevze" # todo renomer cette variable et todo utiliser conf

        # init
        # todo utilser Util
        ImageUtil.clearAndCreatFolder(folderTmp)
        ImageUtil.clearAndCreatFolder(folderTmp2)
        ImageUtil.clearAndCreatFolder(folderOutput)

        # face detection
        print("detection")
        ImageUtil.detectFacesFolder(folderInput, folderTmp)
        if doConvert:
            ImageUtil.detectFacesFolder(folderTmp, folderTmp2)
            # convert
            ImageUtil.convertImage(folderTmp2, folderOutput)
        else:
            ImageUtil.detectFacesFolder(folderTmp, folderOutput)

        # todo utilser Util
        ImageUtil.clearFolder(folderTmp)
        ImageUtil.clearFolder(folderTmp2)

    # todo make comment
    @staticmethod
    def detectFacesFolder(input, output):
        for dirPath, dirNames, files in os.walk(input):
            for dirName in dirNames:
                ImageUtil.detectFacesFolder(dirName, output)

            ImageUtil.detectFacesFiles(dirPath, output)

    # todo make comment
    @staticmethod
    def detectFacesFiles(currentDirectory, output):
        # CASC_PATH = os.path.join( Config().getDetectFacesConfig(["folder"])[0], Config().getDetectFacesConfig(["file"])[0] )
        config = Config()
        CASC_PATH = os.path.join( config.face_detection.folder, config.face_detection.file )
        faceCascade = cv2.CascadeClassifier(CASC_PATH)

        separator = "\\" if sys.platform == "win32" else "/" # todo utilser un separtor global

        # extension = Config().getImageConfig(["extension"])[0]
        extension = config.image.extension

        for dirPath, dirNames, files in os.walk(currentDirectory):
            for file in files:
                if file.startswith("."):
                    continue
                filePath = os.path.join(dirPath,file)
                imgRead = cv2.imread(filePath)

                # GreyScale
                # todo GreyScale n'est plus util on peut le supprimer
                # isColored = Config.getImageConfig(["colored"])[0] != "False"
                isColored = True
                if not isColored:
                    imgRead = cv2.cvtColor(imgRead, cv2.COLOR_BGR2GRAY)

                # Blur
                # todo Bur n'est plus util on peut le supprimer
                # isBlured = Config.getImageConfig(["doBlur"])[0]
                isBlured = False
                if isBlured:
                    imgRead = cv2.blur(imgRead, (3,3))

                # recherche de visage
                try :
                    faces = faceCascade.detectMultiScale(
                        imgRead,
                        scaleFactor=1.1,
                        minNeighbors=5,
                        minSize=(30, 30)
                    )
                except:
                    faces = []

                if len(faces) > 0:
                    destinationFolder = output[:] # sytnaxe python [:] crée une vraie copy pas une copy par référence
                    ImageUtil.createFolder(destinationFolder) # todo utilser Util
                    if dirPath.split(separator)[-1] != currentDirectory:
                        subFolder = dirPath.split(separator)[-1]
                        if subFolder != "originals": # todo magic string a traiter
                            destinationFolder = os.path.join(destinationFolder, subFolder)
                            ImageUtil.createFolder(destinationFolder) # todo utiliser Util
                    img = imgRead
                    count = 0

                    # Draw a rectangle around the faces
                    for (x, y, w, h) in faces:
                        fileName = filePath[filePath.rfind(separator)+1:filePath.index(".")] + "_" + str(count) + extension
                        destinationFile = os.path.join(destinationFolder, fileName)
                        imgRead = imgRead[y:y+h, x:x+w] # realisation du crop
                        # if output != Config().getImagePathConfig(["tmp"])[0]:
                        if output != config.image.path.tmp:
                            # imgRead = cv2.resize(imgRead, (Config().getImageConfig(["width"])[0], Config().getImageConfig(["height"])[0]), interpolation = cv2.INTER_NEAREST)
                            width = config.image.width
                            height = config.image.height
                            imgRead = cv2.resize(imgRead, (width, height), interpolation = cv2.INTER_NEAREST)

                        cv2.imwrite(destinationFile, imgRead)
                        imgRead = img
                        count += 1

    # todo make comment
    @staticmethod
    def splitTrainEval(folderInput, folderOutput):
        print("folderOutput : ", folderOutput)
        ImageUtil.clearAndCreatFolder(folderOutput)

        allImages = ImageUtil.getAllImages(folderInput)
        random.shuffle(allImages)
        print("allImages len : ", len(allImages))

        separator = "\\" if sys.platform == "win32" else "/"

        percentage_train_test = Config.getDeepLearningConfig()[0]
        numberTrain = int( len(allImages) * percentage_train_test / 100 )

        for i in range(len(allImages)):
            imagePath = allImages[i]

            if i < numberTrain:
                folder = os.path.join(folderOutput, "train")
            else:
                folder = os.path.join(folderOutput, "test")
            ImageUtil.createFolder(folder)

            filePath = imagePath.replace(folderInput, folder)

            subFolderName = filePath.split(separator)[-2]
            if subFolderName != "train" and subFolderName != "test":
                subFolder = os.path.join( folder, subFolderName )
                ImageUtil.createFolder( subFolder )
            #print("######")
            #print("imagePath : ", imagePath)
            #print("filePath : ", filePath)
            #exit()
            os.rename(imagePath, filePath)

    # todo make comment
    @staticmethod
    def getAllImages(folder):
        print("getAllImages useless ? use getAllFiles ?")
        images = []
        for dirPath, dirNames, files in os.walk(folder):
            for dirName in dirNames:
                files = ImageUtil.getAllImages(dirName)
                images.extend(files)

            files = ImageUtil.getAllFiles(dirPath)
            images.extend(files)

        print("images : ", len(images))
        exit()
        return images

    # todo make comment
    # todo mettre cette focntion dans Util ?
    @staticmethod
    def getAllFiles(folder):
        ImageUtil.createFolder("files")

        separator = "\\" if sys.platform == "win32" else "/" # todo utiliser un separator global

        # CSV file pour ne pas parcourir tous les dossiers a chaque fois
        filename = folder.split(separator)[-1] + "_files.csv"
        csv_file = os.path.join("files",filename) # todo "files" --> magic number --> utiliser config

        # read file
        if os.path.isfile(csv_file):
            images = []

            ifile  = open(csv_file, "r")
            reader = csv.reader(ifile)

            for row in reader:
                images.extend( row )

            return images

        # create file
        images = []
        for dirPath, dirNames, files in os.walk(folder):
            for file in files:
                if file.startswith("."):
                    continue
                filePath = os.path.join(dirPath,file)

                images.append(filePath)

        # todo utilisé une fonction Util ? voir DataExtractor.writeCSVFile
        file = open(csv_file, "w")
        writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar='\\')
        writer.writerow(images)

        return images
